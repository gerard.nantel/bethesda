package com.gerardnantel.dictionarygenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gerardnantel.dictionarygenerator.Dictionary;
import com.gerardnantel.dictionarygenerator.DictionaryEntry;
import com.gerardnantel.dictionarygenerator.DictionaryFileScanner;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class DictionaryFileScannerTest {
    private DictionaryFileScanner dictionaryFileScanner;
    private ObjectMapper objectMapper;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Before
    public void runBeforeEachTest() {
        // NOTE: we purposefully don't reuse the mapper we use to create test data in the
        // DictionaryFileScanner instance below.
        objectMapper = new XmlMapper();

        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**/*.xml");
        dictionaryFileScanner = new DictionaryFileScanner(pathMatcher, new XmlMapper());
    }

    @Test
    public void shouldCreateValidDictionaryWhenAllFilesAreValid() throws IOException {
        Map<Path, TestDocument> documentsByPath = createValidXmlFiles(10);
        runTest(documentsByPath);
    }

    @Test
    public void shouldCreateValidDictionaryWhenSomeFilesAreInUnexpectedXmlFormat() throws IOException {
        Map<Path, TestDocument> documentsByPath = createValidXmlFiles(6);
        createUnexpectedFormatXmlFiles(4);
        runTest(documentsByPath);
    }

    @Test
    public void shouldCreateValidDictionaryWhenSomeFilesAreInInvalidXmlFormat() throws IOException {
        Map<Path, TestDocument> documentsByPath = createValidXmlFiles(6);
        createInvalidXmlFiles(4);
        runTest(documentsByPath);
    }

    @Test
    public void shouldCreateValidDictionaryWhenSomeFilesAreInRandomFormat() throws IOException {
        Map<Path, TestDocument> documentsByPath = createValidXmlFiles(6);
        createRandomFiles(4);
        runTest(documentsByPath);
    }

    @Test
    public void shouldCreateValidDictionaryAndSkipValidFilesWithUnexpectedExtension() throws IOException {
        Map<Path, TestDocument> documentsByPath = createValidXmlFiles(6);
        createValidXmlFiles(4, "foo");
        runTest(documentsByPath);
    }

    private void runTest(Map<Path, TestDocument> documentsByPath) {
        Dictionary dictionary = dictionaryFileScanner.scan(temporaryFolder.getRoot().toPath());

        for (DictionaryEntry dictionaryEntry : dictionary) {
            TestDocument document = documentsByPath.get(dictionaryEntry.getPathOnServer());
            assertNotNull(document);
            assertEquals(dictionaryEntry.getName(), document.getName());
            assertEquals(dictionaryEntry.getVersion(), document.getVersion());
            assertEquals(dictionaryEntry.getLength(), dictionaryEntry.getPathOnServer().toFile().length());
        }
    }

    private Map<Path, TestDocument> createValidXmlFiles(int count) throws IOException {
        return createValidXmlFiles(count, "xml");
    }

    private Map<Path, TestDocument> createValidXmlFiles(int count, String extension) throws IOException {
        Map<Path, TestDocument> documents = new HashMap<>();

        for (int i = 0; i < count; ++i) {
            File file = temporaryFolder.newFile(Integer.toString(i) + "." + extension);
            TestDocument document = TestDocument.random();
            objectMapper.writeValue(file, document);
            documents.put(file.toPath(), document);
        }

        return documents;
    }

    private void createUnexpectedFormatXmlFiles(int count) throws IOException {
        for (int i = 0; i < count; ++i) {
            createUnexpectedFormatXmlFile(i);
        }
    }

    private void createUnexpectedFormatXmlFile(int i) throws IOException {
        File file = temporaryFolder.newFile("unexpected" + i + ".xml");
        objectMapper.writeValue(file, new RandomObject());
    }

    private void createInvalidXmlFiles(int count) throws IOException {
        for (int i = 0; i < count; ++i) {
            createInvalidXmlFile(i);
        }
    }

    private void createInvalidXmlFile(int i) throws IOException {
        OutputStream outputStream = new FileOutputStream(temporaryFolder.newFile("invalid" + i + ".xml"));
        IOUtils.write(RandomStringUtils.random(1000), outputStream, "UTF-8");
    }

    private void createRandomFiles(int count) throws IOException {
        for (int i = 0; i < count; ++i) {
            createRandomFile(i);
        }
    }

    private void createRandomFile(int i) throws IOException {
        temporaryFolder.newFile("random" + i + ".txt");
    }

    private static class TestDocument {
        @JsonProperty("DOCUMENT_NAME")
        private final String name;
        @JsonProperty("VERSION")
        private final String version;
        @JsonProperty("DATA")
        private final String data;

        private TestDocument(String name, String version, String data) {
            this.name = name;
            this.version = version;
            this.data = data;
        }

        public static TestDocument random() {
            return new TestDocument(RandomStringUtils.randomAlphabetic(10),
                                    RandomStringUtils.randomNumeric(5),
                                    RandomStringUtils.randomAlphanumeric(100));
        }

        public String getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        public String getData() {
            return data;
        }
    }

    private class RandomObject {
        private final String foo;

        public RandomObject() {
            foo = RandomStringUtils.random(100);
        }

        public String getFoo() {
            return foo;
        }
    }
}
