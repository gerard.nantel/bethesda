package com.gerardnantel.dictionarygenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class ClientDictionaryEntrySerializerTest {
    private ObjectMapper serializer;
    private ObjectMapper deserializer;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Before
    public void runBeforeEachTest() {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.addSerializer(DictionaryEntry.class, new ClientDictionaryEntrySerializer());
        serializer = new XmlMapper(xmlModule);
        deserializer = new XmlMapper();
    }

    @Test
    public void shouldSerializeExpectedFields() throws IOException {
        DictionaryEntry entryToSerialize = createDictionaryEntry();
        String serializedEntry = serializer.writeValueAsString(entryToSerialize);
        DictionaryEntry deserializedEntry = deserializer.readValue(serializedEntry, DictionaryEntry.class);
        assertEquals(entryToSerialize.getName(), deserializedEntry.getName());
        assertEquals(entryToSerialize.getVersion(), deserializedEntry.getVersion());
        assertEquals(entryToSerialize.getLength(), deserializedEntry.getLength());
    }

    @Test
    public void shouldNotSerializePathToFileField() throws IOException {
        DictionaryEntry entryToSerialize = createDictionaryEntry();
        String serializedEntry = serializer.writeValueAsString(entryToSerialize);
        DictionaryEntry deserializedEntry = deserializer.readValue(serializedEntry, DictionaryEntry.class);
        assertNull(deserializedEntry.getPathOnServer());
    }

    private DictionaryEntry createDictionaryEntry() throws IOException {
        return DictionaryEntry.create("entryName", "1.2.3", 1234, temporaryFolder.newFile().toPath());
    }
}
