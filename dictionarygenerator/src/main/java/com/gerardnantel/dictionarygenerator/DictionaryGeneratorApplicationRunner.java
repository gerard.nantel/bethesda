package com.gerardnantel.dictionarygenerator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

public class DictionaryGeneratorApplicationRunner implements ApplicationRunner {
    private final DictionaryGenerator dictionaryGenerator;

    public DictionaryGeneratorApplicationRunner(DictionaryGenerator dictionaryGenerator) {
        this.dictionaryGenerator = dictionaryGenerator;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        List<String> nonOptionArgs = applicationArguments.getNonOptionArgs();
        if (nonOptionArgs.size() < 3) {
            throw new IllegalArgumentException("Usage: <sourceFolder> <clientDictionaryFile> <serverDictionaryFile>");
        }

        Path sourceFolder = Paths.get(nonOptionArgs.get(0));
        Path clientDictionaryFile = ensureIsFile(Paths.get(nonOptionArgs.get(1)), "clientDictionary.xml");
        Path serverDictionaryFile = ensureIsFile(Paths.get(nonOptionArgs.get(2)), "serverDictionary.xml");

        dictionaryGenerator.generate(sourceFolder, clientDictionaryFile, serverDictionaryFile);
    }

    private Path ensureIsFile(Path path, String defaultFileName) {
        if (Files.isDirectory(path)) {
            return path.resolve(defaultFileName);
        }
        return path;
    }
}
