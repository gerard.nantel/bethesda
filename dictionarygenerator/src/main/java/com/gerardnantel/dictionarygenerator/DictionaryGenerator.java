package com.gerardnantel.dictionarygenerator;

import java.io.IOException;
import java.nio.file.Path;
import java.text.MessageFormat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

public class DictionaryGenerator {
    private static final Logger log = LogManager.getLogger(ParameterizedMessageFactory.INSTANCE);

    private final DictionaryFileScanner dictionaryFileScanner;
    private final ObjectMapper clientDictionarySerializer;
    private final ObjectMapper serverDictionarySerializer;

    public DictionaryGenerator(DictionaryFileScanner dictionaryFileScanner, ObjectMapper clientDictionarySerializer,
                               ObjectMapper serverDictionarySerializer) {
        this.dictionaryFileScanner = dictionaryFileScanner;
        this.clientDictionarySerializer = clientDictionarySerializer;
        this.serverDictionarySerializer = serverDictionarySerializer;
    }

    public void generate(Path sourceFolder, Path clientDictionaryFile, Path serverDictionaryFile) {
        log.info("Generating server and client dictionaries for {}", sourceFolder);
        Dictionary dictionary = dictionaryFileScanner.scan(sourceFolder);
        serializeClientDictionary(clientDictionaryFile, dictionary);
        serializeServerDictionary(serverDictionaryFile, dictionary);
    }

    private void serializeClientDictionary(Path clientDictionaryFile, Dictionary dictionary) {
        log.info("Serializing client dictionary to {}", clientDictionaryFile);
        serializeDictionary(clientDictionarySerializer, clientDictionaryFile, dictionary);
    }

    private void serializeServerDictionary(Path serverDictionaryFile, Dictionary dictionary) {
        log.info("Serializing server dictionary to {}", serverDictionaryFile);
        serializeDictionary(serverDictionarySerializer, serverDictionaryFile, dictionary);
    }

    private static void serializeDictionary(ObjectMapper serializer, Path file, Dictionary dictionary) {
        try {
            serializer.writeValue(file.toFile(), dictionary);
        } catch (IOException e) {
            throw new IllegalStateException(MessageFormat.format("Error serializing dictionary to file {0}", file), e);
        }
    }
}
