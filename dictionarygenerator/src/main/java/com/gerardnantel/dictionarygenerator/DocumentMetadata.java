package com.gerardnantel.dictionarygenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonIgnoreProperties("DATA")
@JacksonXmlRootElement(localName = "ROOT")
public class DocumentMetadata {
    @JsonProperty("DOCUMENT_NAME")
    private String name;
    @JsonProperty("VERSION")
    private String version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
