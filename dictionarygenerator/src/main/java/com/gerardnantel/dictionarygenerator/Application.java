package com.gerardnantel.dictionarygenerator;

import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Application {
    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DictionaryGeneratorApplicationRunner dictionaryGeneratorApplicationRunner() {
        return new DictionaryGeneratorApplicationRunner(dictionaryGenerator());
    }

    @Bean
    public DictionaryGenerator dictionaryGenerator() {
        return new DictionaryGenerator(dictionaryFileScanner(), clientDictionarySerializer(), defaultObjectMapper());
    }

    @Bean
    public DictionaryFileScanner dictionaryFileScanner() {
        String pathMatcherSyntax = environment.getRequiredProperty("dictionaryFileScanner.pathMatcher");
        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(pathMatcherSyntax);
        return new DictionaryFileScanner(pathMatcher, defaultObjectMapper());
    }

    @Bean
    public ObjectMapper defaultObjectMapper() {
        return new XmlMapper();
    }

    @Bean
    public ObjectMapper clientDictionarySerializer() {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.addSerializer(DictionaryEntry.class, new ClientDictionaryEntrySerializer());
        return new XmlMapper(xmlModule);
    }
}
