package com.gerardnantel.dictionarygenerator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

/**
 * Scans for dictionary files in the given source folder using the PathMatcher provided upon construction.
 */
public class DictionaryFileScanner {
    private static final Logger log = LogManager.getLogger(ParameterizedMessageFactory.INSTANCE);

    private final PathMatcher pathMatcher;
    private final ObjectMapper documentMetadataMapper;

    public DictionaryFileScanner(PathMatcher pathMatcher, ObjectMapper documentMetadataMapper) {
        this.pathMatcher = pathMatcher;
        this.documentMetadataMapper = documentMetadataMapper;
    }

    public Dictionary scan(Path sourceFolder) {
        log.info("Scanning {} for documents", sourceFolder);

        Dictionary dictionary = new Dictionary();
        dictionary.addAll(scanForValidEntries(sourceFolder));
        return dictionary;
    }

    private List<DictionaryEntry> scanForValidEntries(Path sourceFolder) {
        try {
            return Files.find(sourceFolder, Integer.MAX_VALUE, (path, attributes) -> pathMatcher.matches(path))
                        .map(this::createDictionaryEntryForFileIfValid)
                        .flatMap(optionalDictionaryEntry -> optionalDictionaryEntry.map(Stream::of).orElseGet(Stream::empty))
                        .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(MessageFormat.format("Error scanning for files in source folder {0}", sourceFolder), e);
        }
    }

    private Optional<DictionaryEntry> createDictionaryEntryForFileIfValid(Path path) {
        log.info("Creating dictionary entry for document {}", path);

        try {
            DocumentMetadata documentMetadata = documentMetadataMapper.readValue(path.toFile(), DocumentMetadata.class);
            return Optional.of(DictionaryEntry.create(documentMetadata.getName(), documentMetadata.getVersion(), path.toFile().length(), path));
        } catch (IOException e) {
            log.error(MessageFormat.format("Error parsing document metadata from document {0}", path), e);
            return Optional.empty();
        }
    }
}
