package com.gerardnantel.dictionarygenerator;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ClientDictionaryEntrySerializer extends JsonSerializer<DictionaryEntry> {
    @Override
    public void serialize(DictionaryEntry value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();

        gen.writeFieldName("DOCUMENT_NAME");
        gen.writeString(value.getName());

        gen.writeFieldName("VERSION");
        gen.writeString(value.getVersion());

        gen.writeFieldName("LENGTH");
        gen.writeNumber(value.getLength());

        gen.writeEndObject();
    }
}
