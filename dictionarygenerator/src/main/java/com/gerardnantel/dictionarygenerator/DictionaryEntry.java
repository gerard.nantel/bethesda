package com.gerardnantel.dictionarygenerator;

import java.nio.file.Path;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class DictionaryEntry {
    @JsonProperty("DOCUMENT_NAME")
    private String name;
    @JsonProperty("VERSION")
    private String version;
    @JsonProperty("LENGTH")
    private long length;
    @JsonProperty("PATH_TO_FILE")
    private Path pathOnServer;

    static DictionaryEntry create(String name, String version, long length, Path pathOnServer) {
        DictionaryEntry dictionaryEntry = new DictionaryEntry();
        dictionaryEntry.setName(name);
        dictionaryEntry.setVersion(version);
        dictionaryEntry.setLength(length);
        dictionaryEntry.setPathOnServer(pathOnServer);
        return dictionaryEntry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public Path getPathOnServer() {
        return pathOnServer;
    }

    public void setPathOnServer(Path pathOnServer) {
        this.pathOnServer = pathOnServer;
    }
}
