package com.gerardnantel.dictionarygenerator;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Dictionary extends AbstractCollection<DictionaryEntry> {
    private final List<DictionaryEntry> entries = new ArrayList<>();

    @Override
    public boolean add(DictionaryEntry entry) {
        return entries.add(entry);
    }

    @Override
    public Iterator<DictionaryEntry> iterator() {
        return entries.iterator();
    }

    @Override
    public int size() {
        return entries.size();
    }
}
